//**************************************************************
//* タイトル：穴掘り法による迷路の生成
//* 著者：古川湧
//* 日付：2019年4月12日
//* ファイル：meiro1.cpp
//**************************************************************
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "meiro1.hpp"

static int start_p;
static int goal_p;

enum{UP, RIGHT, DOWN, LEFT};

// 壁: 0 道: 1
// STARTは左上
// スタート地点には穴を開ける
int get_start() {return start_p;}
int get_goal() {return goal_p;}
void reset(int a[]) {for (int i = 0; i < N * M; i++) a[i] = 0;}
void set_start(int a[], int p) {a[p] = 1; start_p = p;}
void set_goal(int a[], int p){goal_p = p;}

inline int PX(int pos) {return pos % N;}
inline int PY(int pos) {return pos / N;}


// ランダム関数
int rnd(int min,int max) {
  static int flag = 0;  
  if (flag == 0) {
    srand((unsigned int)time(NULL));
    flag = 1;
    rand();
  }
  return min + (int)(rand() * (max - min + 1.0) / (1.0 + RAND_MAX));
}


//現在の位置posから掘れる道の数を返す.なければ0を返す
static int judge_way(int a[], int pos) {
  int c = 0;
  if (2 <= PY(pos) && a[pos - (N * 2)] == 0)    c++; // up
  if (PX(pos) < N - 2 && a[pos + 2] == 0)       c++; // right
  if (PY(pos) < M - 2 && a[pos + (N * 2)] == 0) c++; // down
  if (2 <= PX(pos) && a[pos - 2] == 0)          c++; // left
  return c;
}

// 進むことができる道を見る。方角を示す配列 way[] を書き換える
static void search_way(int a[], int pos, int way[]) {
  int cnt = 0;
  if (2 <= PY(pos) && a[pos - (N * 2)] == 0)    way[cnt++] = UP;
  if (PX(pos) < N - 2 && a[pos + 2] == 0)       way[cnt++] = RIGHT;
  if (PY(pos) < M - 2 && a[pos + (N * 2)] == 0) way[cnt++] = DOWN;
  if (2 <= PX(pos) && a[pos - 2] == 0)          way[cnt++] = LEFT;
}

//一マス先とニマス先を道(0)にする。進んだマスを値で返す。
static int set_way(int a[], int pos, int v) {
  int new_pos = 0;
  if (v == UP) {
    a[pos - N * 2] = a[pos - N * 1] = 1;
    new_pos = pos - N * 2;
  } else if (v == RIGHT) {
    a[pos + 1] = a[pos + 2] = 1;
    new_pos = pos + 2;
  } else if (v == DOWN) {
    a[pos + (N * 2)] = a[pos + (N * 1)] = 1;
    new_pos = pos + (N * 2);
  } else if (v == LEFT) {
    a[pos - 2] = a[pos - 1] = 1;
    new_pos = pos - 2;
  } else {   // エラー処理ここから
    printf("\n[error]from set_way wrong [v = %d]\n",v);
    display(a);
    exit(1);
  }          //　エラー処理ここまで
  return new_pos;
}

//空いている道からランダムに道を選ぶ.進んだnew_posを返す
static int make_way(int a[], int pos) {
  int way[4] = {0};
  search_way(a, pos, way);
  int r = rnd(0, judge_way(a, pos) - 1); //進めるマスからランダムに道を選ぶ
  int new_pos = set_way(a, pos, way[r]);
  return new_pos;
}

//迷路を作る。
void make_labyrinth(int a[], int pos) {
  if (judge_way(a, pos) != 0) {
    int next_pos = make_way(a, pos);
    make_labyrinth(a, next_pos);  
    make_labyrinth(a, pos);
  }
}

inline void myputs(const char *s) {printf("%s", s);}
inline void NL() {myputs("\n");}

//表示関数
void display(int a[]) {
  NL();
  for (int i = 0; i < M; i++) {
    for (int j = 0; j < N; j++) {
      if (a[i * N + j] == 0) myputs("##");
      else if (i * N + j == start_p) myputs("S ");
      else if (i * N + j == goal_p) myputs(" G");
      else myputs("  ");
    }
    NL();
  }
  NL();
}

