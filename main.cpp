//**************************************************************
//* タイトル：穴掘り法による迷路の生成
//* 著者：古川湧
//* 日付：2019年4月12日
//* ファイル：main.cpp
//**************************************************************
#include "meiro1.hpp"

int main(int argc, char *argv[]) {
  int arry[N * M];  // 迷路の盤　 壁: 0  道: 1
  reset(arry);
  set_start(arry, N * 1 + 1);
  set_goal(arry, N * (M - 1) - 2);
  make_labyrinth(arry, get_start());
  display(arry);
  return 0;
}
