//**************************************************************
//* タイトル：穴掘り法による迷路の生成
//* 著者：古川湧
//* 日付：2019年4月12日
//* ファイル：meiro1.hpp
//**************************************************************

static const int N = 15;  //盤の横幅
static const int M = 17;  //盤の縦幅


int get_start();
int get_goal();
void reset(int a[]);
void set_start(int a[], int p);
void set_goal(int a[], int p);
void make_labyrinth(int a[], int pos);
void display(int a[]);

