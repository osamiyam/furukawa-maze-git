
CXX = g++
CXXFLAGS = -Wall
LDFLAGS = -lstdc++ -lm

all: meiro1

main.o: main.cpp meiro1.hpp
meiro1.o: meiro1.cpp meiro1.hpp
meiro1: meiro1.o main.o

clean:
	rm -f *.o meiro1 *~
